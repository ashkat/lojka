<?php
// HTTP
define('HTTP_SERVER', 'http://lojka.by/admin/');
define('HTTP_CATALOG', 'http://lojka.by/');

// HTTPS
define('HTTPS_SERVER', 'http://lojka.by/admin/');
define('HTTPS_CATALOG', 'http://lojka.by/');

// DIR
define('DIR_APPLICATION', '/home/u590081130/public_html/admin/');
define('DIR_SYSTEM', '/home/u590081130/public_html/system/');
define('DIR_LANGUAGE', '/home/u590081130/public_html/admin/language/');
define('DIR_TEMPLATE', '/home/u590081130/public_html/admin/view/template/');
define('DIR_CONFIG', '/home/u590081130/public_html/system/config/');
define('DIR_IMAGE', '/home/u590081130/public_html/image/');
define('DIR_CACHE', '/home/u590081130/public_html/system/cache/');
define('DIR_DOWNLOAD', '/home/u590081130/public_html/system/download/');
define('DIR_UPLOAD', '/home/u590081130/public_html/system/upload/');
define('DIR_LOGS', '/home/u590081130/public_html/system/logs/');
define('DIR_MODIFICATION', '/home/u590081130/public_html/system/modification/');
define('DIR_CATALOG', '/home/u590081130/public_html/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'mysql.hostinger.ru');
define('DB_USERNAME', 'u590081130_root');
define('DB_PASSWORD', '123456');
define('DB_DATABASE', 'u590081130_lojka');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
