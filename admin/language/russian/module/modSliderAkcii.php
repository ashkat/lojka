<?php
// Heading
$_['heading_title']    = 'Слайдер и Акции';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Модуль Слайдер и Акции успешно изменен!';
$_['text_edit']        = 'Редактировать модуль Слайдер и Акции';

// Entry
$_['entry_name']     = 'Название';
$_['entry_banner']     = 'Банер';
$_['entry_width']     = 'Длина';
$_['entry_height']     = 'Высота';
$_['entry_status']     = 'Статус';


// Error
$_['error_permission'] = 'У вас нет прав для изменения модуля Слайдер и Акции!';