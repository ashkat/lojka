<?php
// Heading
$_['heading_title']    = 'Стена категорий';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Модуль успешно изменен!';
$_['text_edit']        = 'Изменить модуль';

// Entry
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У вас недостаточно прав для редактирования модуля';