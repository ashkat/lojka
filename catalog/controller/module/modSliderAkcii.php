<?php
class ControllerModulemodSliderAkcii extends Controller {
	public function index() {
		/*static $module = 0;
		$this->document->addStyle('css/style.css');
		$this->document->addScript('catalog/view/javascript/bootstrap/js/bootstrap.min.js');
		$data['module'] = $module++;*/
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/modSliderAkcii.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/modSliderAkcii.tpl');
		} else {
			return $this->load->view('default/template/module/modSliderAkcii.tpl');
		}
	}
}
?>