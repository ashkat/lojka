<?php echo $header; ?>
<section id="toppos">

  <div class="container-fluid">

      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>

    <div class="row"><?php echo $column_left; ?>

      <?php if ($column_left && $column_right) { ?>
      <?php $class = 'col-sm-6'; ?>
      <?php } elseif ($column_left || $column_right) { ?>
      <?php $class = 'col-lg-10 col-md-10 col-sm-9 pl-0'; ?>
      <?php } else { ?>
      <?php $class = 'col-sm-12'; ?>
      <?php } ?>

    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h2><?php echo $heading_title; ?></h2>
      <?php if ($thumb || $description) { ?>

      <div class="row">
          <?php if ($thumb) { ?>
          <div class="col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
          <?php } ?>
          <?php if ($description) { ?>
          <div class="col-sm-10 descript"><?php echo $description; ?></div>
          <?php } ?>
      </div>

      <hr>

      <?php } ?>

    <!--  <?php if ($categories) { ?>
      <h3><?php echo $text_refine; ?></h3>
      <?php if (count($categories) <= 5) { ?>

      <div class="row">
          <div class="col-sm-3">
            <ul>
              <?php foreach ($categories as $category) { ?>
              <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
              <?php } ?>
            </ul>
          </div>
      </div>

      <?php } else { ?>

      <div class="row">
          <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
          <div class="col-sm-3">
            <ul>
              <?php foreach ($categories as $category) { ?>
              <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
              <?php } ?>
            </ul>
          </div>
          <?php } ?>
      </div>

        <?php } ?>
        <?php } ?> -->


        <?php if ($products) { ?>
       <!-- <p><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></p> -->

      <div class="row">
            <div class="col-md-4">
              <div class="btn-group hidden-xs">
                <a type="button" id="list-view" onclick="$('.descript-product').show();" class="btn btn-sm btn-white" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></a>
                <a type="button" id="grid-view" onclick="$('.descript-product').hide();" class="btn btn-sm btn-white" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></a>
              </div>
            </div>

            <div class="col-md-2 text-right">
              <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
            </div>

            <div class="col-md-3 text-right">
              <select id="input-sort" class="form-control" onchange="location = this.value;">
                <?php foreach ($sorts as $sorts) { ?>
                <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>

            <div class="col-md-1 text-right">
              <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
            </div>

            <div class="col-md-2 text-right">
              <select id="input-limit" class="form-control" onchange="location = this.value;">
                <?php foreach ($limits as $limits) { ?>
                <?php if ($limits['value'] == $limit) { ?>
                <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>

      </div>

      <hr class="mt-10">

      <div class="row">
          <?php $j=0;?>
        <?php foreach ($products as $product) { ?>

        <div class="product-layout product-grid col-sm-12">

          <figure class="mnblock-prod product-thumb">
            <input type="hidden" name="product_id" value="<?php echo $product['product_id']; ?>"/>

            <a href="<?php echo $product['href']; ?>" class="img-link image" data-toggle="modal" data-target="#modalProduct">
              <img class="img-responsive" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>">
            </a>

            <div class="label-heart"><a type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></a></div>

            <figcaption class="caption">
              <a href="<?php echo $product['href']; ?>" class="title-link" data-toggle="modal" data-target="#modalProduct">
                <h4><?php echo $product['name']; ?></h4>
              </a>

                <p class="descript-product"><?php echo $product['description']; ?></p>

                <?php if ($product['rating']) { ?>

                <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                <?php } ?>
                <?php if ($product['price']) { ?>

                <p class="price">
                  <?php if (!$product['special']) { ?>
                  <span class="price_mn"><?php echo $product['price']; ?></span> <!--<i class="fa fa-rub"></i>-->
                  <input type="hidden" class="product_price" value="<?php echo $product['price']; ?>">
                  <?php } else { ?>
                  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                  <?php } ?>
                  <?php if ($product['tax']) { ?>
                  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>

              <div class="row mr-ml-0imp mnqntity-input">

                  <div class="col-md-12">
                    <div class="qntity-input pull-left">
                      <a class="qntity-iminus" onclick="changeQty('qntity_order<?php echo $j;?>',0); return false;"><i class="fa fa-minus"></i></a>
                      <input class="quantity" id="qntity_order<?php echo $j;?>" type="text" value="1" maxlength="2" required="required" data-toggle="tooltip" data-placement="top" title="Количество" data-original-title="Количество">
                      <a class="qntity-iplus" onclick="changeQty('qntity_order<?php echo $j;?>',1); return false;"><i class="fa fa-plus"></i></a>
                    </div>
                  </div>
                  <?php $j+=1;?>

              </div>

              <div class="row mr-ml-0imp">

                  <?php if (isset($product['options'])) { ?>
                  <?php foreach ($product['options'] as $option) { ?>

                  <?php if (isset($option['type']) && $option['type'] == 'radio') { ?>
                  <div id="product" class="col-md-9 col-sm-9 col-xs-9 pr-0 mb-10">

                      <div class="mnmukgg-qntity form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                          <div id="input-option<?php echo $option['product_option_id']; ?>" class="mukgg-qntity btn-group pull-left" data-toggle="buttons">
                                <?php foreach ($option['product_option_value'] as $option_value) { ?>

                                    <label class="btn btn-sm btn-white">
                                        <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" class="product_option" value="<?php echo $option_value['product_option_value_id']; ?>">
                                        <?php echo $option_value['name']; ?>
                                        <!--  <?php if ($option_value['price']) { ?>
                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                         <?php } ?>-->
                                        <?php if ($option_value['price']) { ?>
                                          <input type="hidden" class="opt_price_val" value="<?php echo $option_value['price']; ?>">
                                        <?php } ?>
                                    </label>

                                <?php } ?>
                          </div>
                      </div>

                  </div>
                  <?php } ?>
                  <?php } ?>
                  <?php } ?>

                  <div class="col-md-3 col-sm-3 col-xs-3 mncart-plus">
                    <div class="cart-plus pull-right">
                       <!-- <a type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="fa fa-cart-plus"></i></a>-->
                       <!-- <a type="button" id="button-cart"><i class="fa fa-cart-plus"></i></a>-->
                        <a class="button-cart" type="button"><i class="fa fa-cart-plus"></i></a>
                    </div>
                  </div>

              </div>

            </figcaption>

          </figure>

        </div>

        <?php } ?>

      </div>


      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
          <?php } ?>
          <?php if (!$categories && !$products) { ?>
          <p><?php echo $text_empty; ?></p>
              <div class="buttons">
                <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
              </div>
      <?php } ?>
      <?php echo $content_bottom; ?><!--</div>-->
    <?php echo $column_right; ?><!--</div>-->

  </div>

</section>

<?php echo $footer; ?>

<div class="modal fade" id="modalProduct" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">


        </div>
    </div>
</div>

<script type="text/javascript">





    $(document).ready(function() {

    $('#modalProduct').on('hide.bs.modal', function() {
        $(this).removeData();
    });


    $(".mukgg-qntity label:first" , "figure").addClass("active");
    $(".mukgg-qntity input:first" , "figure").attr('checked', true);


    $('.product_option').change(function() {

        var figure = $($(this).parents("figure").get(0));
        var quantity = figure.find("input.quantity").get(0).value;
        var product_price = figure.find(".product_price").get(0).value;
        var opt_price_val = $($(this).parent()).find(".opt_price_val").get(0).value;
        var lprice = 0;

        //lprice = quantity * (parseInt(product_price) + parseInt(opt_price_val));
        lprice = quantity * (parseFloat(product_price) + parseFloat(opt_price_val));

        figure.find(".price_mn").html(lprice + ' руб'); //html(parsePrice(lonprice)+' руб');

    });

     $('.qntity-input a').on("click", function() {

            var figure = $($(this).parents("figure").get(0));
            var quantity = figure.find("input.quantity").get(0).value;
            var product_price = figure.find(".product_price").get(0).value;
            var opt_price_val = $(figure).find("label.active .product_option").siblings(".opt_price_val").val();
            var lprice = 0;
          //  var lonprice = 0;


            if ( $(".mukgg-qntity label:first").is(".active"))
            { lprice = quantity * parseFloat(product_price);
              figure.find(".price_mn").html(parseFloat(lprice) + ' руб'); //html(parsePrice(lonprice)+' руб');
            }
            else if ( $(".mukgg-qntity label:first").not(".active")) //last").is(".active"))
            { lprice = quantity * (parseFloat(product_price) + (parseFloat(opt_price_val)));
              figure.find(".price_mn").html(parseFloat(lprice) + ' руб'); //html(parsePrice(lonprice)+' руб');
            }

            });


    /*    function parsePrice(pnum){

       if (pnum.toString().length == 4)
        {pnum = pnum.toString().substring(0,1)+","+pnum.toString().substring(1,4); return pnum;}

       else if (pnum.toString().length == 5)
        {pnum = pnum.toString().substring(0,2)+","+pnum.toString().substring(2,5); return pnum;}

       else if (pnum.toString().length == 6)
        {pnum = pnum.toString().substring(0,3)+","+pnum.toString().substring(3,6); return pnum;}

        else if (pnum.toString().length == 7)
        { pnum = pnum.toString().substring(0,1)+","+pnum.toString().substring(1,4)+","+pnum.toString().substring(4,7); return pnum;}

        else if (pnum.toString().length == 8)
        { pnum = pnum.toString().substring(0,2)+","+pnum.toString().substring(2,5)+","+pnum.toString().substring(5,8); return pnum;}

        }*/




    });


$('.button-cart').on('click', function() {

        var figure = $($(this).parents("figure").get(0));
        var product_id = figure.find("input[name=product_id]").get(0).value;
        var quantity = figure.find("input.quantity").get(0).value;
        var options = $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked').serializeArray();

        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1) + '&' + $.param(options),
//           data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
//            data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked'),
            dataType: 'json',
            beforeSend: function () {
                $('#cart > a').button('loading');
            },
            complete: function () {
                $('#cart > a').button('reset');
            },
            success: function (json) {

                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    //alert('ok');
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        $('#cart > a').html('<i class="fa fa-shopping-cart shop-cart pr-5"></i><span id="cart-total" class="badge ml-5">' + json['total'] + '</span>');
                    }, 100);

                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            }
        });
});


//$(document).on('keyup','[name=\'quantity\']', getprice {
//$(document).on('change','[name=\'option[\']', getprice());
//$(document).on('change','input[type=\'radio\']', function() {


</script>


