﻿<a href="#" class="scrollup" style="display: block;">
	<i class="fa fa-angle-up"></i>
</a>


<footer>
	<section class="footer" id="footer">
        <div class="footer-top">
            <div class="container-fluid">
                <div class="row">


                    <div class="col-md-4 col-sm-4 wow fadeInUpSmall mb-20">
                    				  <div class="headline">
                    					  <h3 class="titlemn">Как нас найти</h3>
                    				  </div>

                    				  <h4 class="title"><strong>Кондитерский Дом "Шоколадная Ложка"</strong></h4>
                    				  <ul class="list">
                    					  <li class="mb-5"><i class="fa fa-home pr-10"></i>г. Минск, ул. Веры Хоружей, дом 13</li>
                    					  <li class="mb-5"><i class="fa fa-phone pr-10"></i>(8017) 334 34 32</li>
                    					  <li class="mb-5"><i class="fa fa-mobile pr-10 pl-5"></i>(8044) 593 30 06</li>
                    					  <li class="mb-5"><i class="fa fa-envelope pr-10"></i><a class="mailcolor" href="mailto:delovarservice@mail.ru">delovarservice@mail.ru</a></li>
                    				  </ul>

                    				  <ul class="social-links">
                    					  <li class="vkontakte"><a data-placement="top" title="" data-original-title="Вконтакте" target="_blank" href="http://www.vk.com">
                    						  <i class="fa fa-vk"></i></a></li>
                    					  <li class="facebook"><a data-placement="top" title="" data-original-title="Facebook" target="_blank" href="https://www.facebook.com/">
                    						  <i class="fa fa-facebook"></i></a></li>
                    					  <li class="twitter"><a data-placement="top" title="" data-original-title="Twitter" target="_blank" href="https://twitter.com/">
                    						  <i class="fa fa-twitter"></i></a></li>
                    					  <li class="googleplus"><a data-placement="top" title="" data-original-title="GooglePlus" target="_blank" href="http://plus.google.com">
                    						  <i class="fa fa-google-plus"></i></a></li>
                    					  <li class="skype"><a data-placement="top" title="" data-original-title="Skype" target="_blank" href="http://www.skype.com">
                    						  <i class="fa fa-skype"></i></a></li>
                    					  <li class="linkedin"><a data-placement="top" title="" data-original-title="LinkedIn" target="_blank" href="http://www.linkedin.com">
                    						  <i class="fa fa-linkedin"></i></a></li>
                    					  <li class="youtube"><a data-placement="top" title="" data-original-title="YouTube" target="_blank" href="http://www.youtube.com">
                    						  <i class="fa fa-youtube"></i></a></li>
                    					  <!--<li class="flickr"><a target="_blank" href="http://www.flickr.com"><i class="fa fa-flickr"></i></a></li>
                    					  <li class="pinterest"><a target="_blank" href="http://www.pinterest.com"><i class="fa fa-pinterest"></i></a></li>-->
                    				  </ul>
                    </div>

                    <div class="col-md-4 col-sm-4 wow fadeInUpSmall mb-20">
                    					 <div class="headline">
                    						 <h3 class="titlemn">Как связаться с нами</h3>
                    					 </div>
                    					 <form id="footer-contform" role="form" novalidate="novalidate">
                    						 <div class="form-group has-feedback">
                    							 <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Ф.И.О." data-toggle="tooltip" data-placement="top" title="" data-original-title="Введите Ф.И.О.">
                    							 <i class="fa fa-user form-control-feedback form-isearch-nstl"></i>
                    						 </div>
                    						 <div class="form-group has-feedback">
                    							 <input type="email" class="form-control" id="email" name="email" placeholder="Email" data-toggle="tooltip" data-placement="top" title="" data-original-title="Введите электронный адрес">
                    							 <i class="fa fa-envelope form-control-feedback form-isearch-nstl"></i>
                    						 </div>
                    						 <div class="form-group has-feedback">
                    							 <input type="text" class="form-control" id="phone_num" name="phone_num" placeholder="Телефон" data-toggle="tooltip" data-placement="top" title="" data-original-title="Введите номер телефона">
                    							 <i class="fa fa-phone form-control-feedback form-isearch-nstl"></i>
                    						 </div>

											 <script type="text/javascript">
												 $(function($){
													 $("#phone_num").mask("+375 (99) 999-9999");
												 });
											 </script>

                    						 <div class="form-group has-feedback">
                    							 <textarea rows="6" class="form-control" id="comments" name="comments" placeholder="Ваш вопрос или комментарий" data-toggle="tooltip" data-placement="top" title="" data-original-title="Введите текст сообщения"></textarea>
                    							 <i class="fa fa-pencil form-control-feedback form-isearch-nstl"></i>
                    						 </div>

                    					 </form>

                            <div class="clearfix">
                                <a href="#" type="submit" class="btn-brown btn-sm pull-right"><i class="fa fa-paper-plane pr-5"></i>Отправить</a>
                            </div>

                    </div>

                    <div class="col-md-4 col-sm-4 wow fadeInUpSmall mb-20">
                    				  <div class="headline">
                    					  <h3 class="titlemn">Подписка</h3>
                    				  </div>
                    				  <p>Подпишитесь на наши новости один раз и вы будете в курсе всех событий компании.</p>
                    				  <form id="subscr-form" role="form" novalidate="novalidate">
                    					  <div class="form-group has-feedback">
                    						  <input type="email" class="form-control" id="subscribe" placeholder="Введите email" name="subscribe" data-toggle="tooltip" data-placement="top" title="" data-original-title="Введите электронный адрес">
                    						  <i class="fa fa-envelope form-control-feedback form-isearch-nstl"></i>
                    					  </div>

                    				  </form>

                        <div class="clearfix">
                            <a href="#" type="submit" class="btn-brown btn-sm pull-right mb-10"><i class="fa fa-paper-plane pr-5"></i>Подписаться</a>
                        </div>

                    </div>

                </div>
            </div>
        </div>



		    <div class="footer-bottom">
	        <div class="container-fluid">
				<div class="row">

					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<p class="pull-left">© Все права защищены. Использование материалов сайта только с согласия владельца. <br> ООО «Кондитерский дом «Шоколадная ложка» 2015.<br><br><small>Разработка <a href="http://www.indadj.com">indadj.com</a></small></p>

					</div>

					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 torg-znak">

						<p><a href="publicoferta.php" target="_blank">Публичный договор</a> | <a href="privacypolicy.php" target="_blank">Политикой конфиденциальности</a></p>

						<div class="payments-card">
						<ul class="nav nav-pills payments">
							<li class="pr-10"><i class="fa fa-opencart"></i></li>
							<li><i class="fa fa-cc-visa"></i></li>
							<li><i class="fa fa-cc-mastercard"></i></li>
							<li><i class="fa fa-cc-amex"></i></li>
							<li><i class="fa fa-cc-paypal"></i></li>
						</ul>
						</div>
	            	</div>
				</div>


	        </div>
            </div>
    </section>

</footer>


<script type="text/javascript">
$(document).ready(function() {

    //Wow
    new WOW().init();

		 /* affix the navbar after scroll below header */
$('#nav').affix({offset: { top: $('header').height()-$('#nav').height() } });
/* highlight the top nav as scrolling occurs */
$('body').scrollspy({ target: '#nav' });
/* smooth scrolling for nav sections */
$('#hnav .navbar-nav li>a').click(function(){
  var link = $(this).attr('href');
  var posi = $(link).offset().top+20;
$('body,html').animate({scrollTop:posi},700); });

	//scroll to top
			$(window).scroll(function(){
				if ($(this).scrollTop() > 100) {
					$('.scrollup').fadeIn();
					} else {
					$('.scrollup').fadeOut();
				}
			});
			$('.scrollup').click(function(){
				$("html, body").animate({ scrollTop: 0 }, 1000);
					return false;
			});


	//Formstone
   // $("input[type=checkbox] ,input[type=radio]").checkbox({});
    $("input[type=checkbox]").checkbox({});
   // $('select').dropdown({});

	// tooltip
	$('.social-links li a, input, textarea, select, label, a').tooltip();



});

</script>

</body></html>