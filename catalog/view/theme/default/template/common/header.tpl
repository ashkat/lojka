﻿<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<!--<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>-->
    <script type="text/javascript" src="js/jquery-2.1.4.js"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!--<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />-->
  <link rel="stylesheet" href="catalog/view/javascript/bootstrap/css/bootstrap-theme.css" media="screen" >
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/animate.css">
  <link rel="stylesheet" href="css/checkbox.css">
  <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/stylesheet.css" >

  <script type="text/javascript" src="js/wow.min.js"></script>
  <script type="text/javascript" src="js/core.js"></script>
  <script type="text/javascript" src="js/touch.js"></script>
  <script type="text/javascript" src="js/checkbox.js"></script>
  <script type="text/javascript" src="js/maskedinput.js"></script>

<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php echo $google_analytics; ?>
</head>
<body class="<?php echo $class; ?>">

<header id="hnav" class="navbar navbar-default">

    <div class="container-fluid">
    	        <div class="row">

    	            <div class="col-lg-4 col-md-3 hidden-sm hidden-xs wow fadeInLeftSmall">
    	            	<div class="logomain">
                            <a href="<?php echo $home; ?>"><img class="header-logo" src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt=Кондитерский дом "Шоколадная ложка"></a>
    	            	</div>
    	            </div>

                    <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                    </div>

                    <div class="col-lg-4 col-md-5 hidden-sm hidden-xs shotinfo">
                        <ul class="shot-bar-menu pull-right">
                            <li><i class="fa fa-phone"></i><strong>(8044) 593 30 06</strong></li>
                            <li><i class="fa fa-envelope"></i><a href="#">delovarservice@mail.ru</a></li>
                        </ul>
                    </div>

                </div>
    </div>

    <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="true">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                 <!--   <a href="index.php"><img class="header-logo" src="images/logo200.png" alt=Кондитерский дом "Шоколадная ложка"></a>-->
                </div>

        <!-- 0 => array(0 => '/pay/index.php', 1=> ''),
                        1 => array(0 => '/pay/menu.php', 1=> 'Меню'),
                        2 => array(0 => '/pay/shops.php', 1=> 'Магазины'),
                        3 => array(0 => '/pay/delivery.php', 1=> 'Доставка'),
                        4 => array(0 => '/pay/about.php', 1=> 'О Нас'),
                        5 => array(0 => '/pay/contact.php', 1=> 'Контакты')   -->

        <nav class="navbar-collapse collapse pr-0 pl-0" aria-expanded="true">
            <ul class="nav navbar-nav nav-left menuul">
                <?php
                $menu = array(

                    0 => array(0 => '/index.php', 1=> ''),
                    1 => array(0 => '//www.lojka.by/index.php?route=information/sitemap', 1=> 'Меню'),
                    2 => array(0 => '//www.lojka.by/index.php?route=information/information&information_id=5', 1=> 'Магазины'),
                    3 => array(0 => '//www.lojka.by/index.php?route=information/information&information_id=6', 1=> 'Доставка'),
                    4 => array(0 => '//www.lojka.by/index.php?route=information/information&information_id=4', 1=> 'О Нас'),
                    5 => array(0 => '//www.lojka.by/index.php?route=information/contact', 1=> 'Контакты')

                );
                foreach ($menu as $item):
                ?>
                <li <?=$_SERVER['REQUEST_URI'] == $item[0]?'class="active"':'' ?>>
                    <a href="<?=$item[0]?>" <?=$_SERVER['REQUEST_URI'] == $item[0] ? 'class="active"':''?>>

                        <?= $item[1] == '' ? '<i class="fa fa-home fai-home-nstl"></i>' : '' ?>

                        <?=$item[1]?></a></li>
                <?php endforeach;?>

            </ul>

         <!-- <?php if(!$logged) : ?>-->

                    <ul class="nav navbar-nav menuul nav-right navbar-right">

                        <?php echo $cart; ?>

                        <li class="dropdown">
                            <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#" aria-expanded="false">
                                <i class="fa fa-lock pr-5"></i> Вход <span class="caret"></span></a>
                            <ul class="dropdown-menu animated fadeInDownSmall" data-animation-effect="fadeInDownSmall" data-effect-delay="600">
                                <li class="ul-drdn-nstl">
                                    <form class="login-form" method="post">

                                        <div class="form-group fg-form-nstl has-feedback">
                                            <input type="text" class="form-control form-control-nstl" placeholder="Email">
                                            <i class="fa fa-envelope form-control-feedback form-isearch-nstl"></i>
                                        </div>

                                        <div class="form-group fg-form-nstl has-feedback">
                                            <input type="password" class="form-control form-control-nstl" placeholder="Пароль">
                                            <i class="fa fa-lock form-control-feedback form-isearch-nstl"></i>
                                        </div>

                                        <div class="form-group ">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> Запомнить
                                                </label>
                                            </div>
                                        </div>

                                        <div class="clearfix">
                                            <a class="btn-sm btn-brown" href="#" type="submit" role="button">Войти</a>
                                            <a class="btn-sm btn-brown pull-right" href="#" type="submit" role="button">Забыли пароль?</a>
                                        </div>


                                        <div class="divider fg-form-nstl"></div>
                                        <span class="text-center">Войти через</span>
                                        <ul class="social-links clearfix">
                                            <li class="vkontakte"><a target="_blank" href="http://www.vk.com">
                                                    <i class="fa fa-vk sl-liai-nstl"></i></a></li>
                                            <li class="facebook"><a target="_blank" href="http://www.facebook.com">
                                                    <i class="fa fa-facebook sl-liai-nstl"></i></a></li>
                                            <li class="twitter"><a target="_blank" href="http://www.twitter.com">
                                                    <i class="fa fa-twitter sl-liai-nstl"></i></a></li>
                                            <li class="googleplus"><a target="_blank" href="http://plus.google.com">
                                                    <i class="fa fa-google-plus sl-liai-nstl"></i></a></li>
                                        </ul>

                                        <div class="divider"></div>
                                        <div class="text-center">
                                            <a class="btn-sm btn-brown" href="registr.php" type="submit" role="button"><i class="fa fa-user pr-10"></i>Регистрация</a>
                                        </div>

                                    </form>
                                </li>
                            </ul>
                        </li>


                    </ul>


       <!--   <?php else :?>

                <ul class="nav navbar-nav menuul navbar-right"> -->   <!--<?=$user['name'];?>-->
           <!--  <li>
                        <a href="#"> <span class="hidden-xs">Имя пользователя </span> <i class="fa fa-shopping-cart shop-cart pr-10"></i><span class="badge badge-success">5</span></a>
                    </li>
                </ul>

          <?php endif; ?> -->
        </nav>



    </div>


</header>
