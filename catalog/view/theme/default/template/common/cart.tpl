<li id="cart" class="dropdown">

  <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#" aria-expanded="false" data-loading-text="<?php echo $text_loading; ?>">
    <i class="fa fa-shopping-cart shop-cart pr-5"></i>
    <span id="cart-total" class="badge ml-5"><?php echo $text_items; ?></span></a>
  <ul class="dropdown-menu animated fadeInDownSmall" data-animation-effect="fadeInDownSmall" data-effect-delay="600">

      <?php if ($products || $vouchers) { ?>

    <li class="ul-drdn-cart">

      <form class="mincart-form" method="post" enctype="multipart/form-data">

        <div class="cart-items">

          <table>
              <?php $q=0;?>
            <?php foreach ($products as $product) { ?>

            <tbody>

            <tr class="miniCartProduct">
              <td class="miniCartProductThumb">
                  <?php if ($product['thumb']) { ?>
                  <a href="<?php echo $product['href']; ?>" data-toggle="modal" data-target="#modalProduct">
                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>">
                  </a>
                  <?php } ?>
              </td>

              <td style="width:30%">
                <div class="miniCartDesc">
                 <!-- <p>Торт</p>-->
                  <h6><a href="<?php echo $product['href']; ?>" data-toggle="modal" data-target="#modalProduct">
                      <strong><?php echo $product['name']; ?></strong></a></h6>

                    <?php if ($product['option']) { ?>
                    <?php foreach ($product['option'] as $option) { ?>
                    <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small>
                    <?php } ?>
                    <?php } ?>
                    <?php if ($product['recurring']) { ?>
                    <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
                    <?php } ?>

                </div>


              </td>

                <td style="width:15%" class="miniCartQuantity text-center">
                    <div class="btn-group">
                        <a onclick="changeQty('cart-quantity<?php echo $q;?>',0); return false;">
                            <i class="fa fa-minus mr-5"></i>
                        </a>
                        <input id="cart-quantity<?php echo $q;?>" type="text" size="2" value="<?php echo $product['quantity']; ?>" min="0" maxlength="2" required="required" data-placement="bottom" data-original-title="Число">
                        <a onclick="changeQty('cart-quantity<?php echo $q;?>',1); return false;">
                            <i class="fa fa-plus ml-5"></i>
                        </a>
                    </div>
                    <?php $q+=1;?>
                </td>

                <td style="width:20%" class="miniCartSubtotal text-center">
                    <span><?php echo $product['total']; ?></span>
                </td>

                <td style="width:15%" class="miniCartDel text-center">
                    <a type="button" onclick="cart.remove('<?php echo $product['key']; ?>');" title="<?php echo $button_remove; ?>"><i class="fa fa-trash-o"></i></a>
                </td>

            </tr>

            <?php } ?>

            <?php foreach ($vouchers as $voucher) { ?>
            <tr>
                <td class="text-center"></td>
                <td class="text-left"><?php echo $voucher['description']; ?></td>
                <td class="text-right">x&nbsp;1</td>
                <td class="text-right"><?php echo $voucher['amount']; ?></td>
                <td class="text-center text-danger"><a type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a></td>
            </tr>
            <?php } ?>


            </tbody>
              <tfoot>
              <?php foreach ($totals as $total) { ?>
              <tr class="cart-total">
                  <td colspan="3" class="ctotal-tov text-right"><small><?php echo $total['title']; ?></small></td>
                  <td colspan="2" class="ctotal-price text-right"><small><?php echo $total['text']; ?></small></td>
              </tr>
              <?php } ?>
              </tfoot>
          </table>
        </div>

          <div class="clearfix mt-5">
              <a href="<?php echo $cart; ?>" class="pull-left btn-brown btn-sm" role="button"><i class="fa fa-shopping-cart mr-5"></i><strong><?php echo $text_cart; ?></strong></a>

              <a href="<?php echo $checkout; ?>" class="pull-right btn-brown btn-sm ml-10" role="button"><i class="fa fa-share mr-5"></i><strong><?php echo $text_checkout; ?></strong></a>
          </div>

      </form>
    </li>

      <?php } else { ?>

      <li>
          <p class="text-center"><?php echo $text_empty; ?></p>
      </li>

      <?php } ?>

  </ul>
</li>