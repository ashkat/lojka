
<section id="asort">
    <div class="container-fluid">
        <div class="row"> <!--style="opacity: 1; display: block;margin-bottom: 0px;"-->

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="headline">
                    <h3><?php echo $heading_title; ?></h3>
                </div>
            </div>

            <?php foreach ($categories as $category) { ?>

              <!--  <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 wow fadeInUpSmall">
                    <div class="product-thumb transition">
                        <div class="image"><a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="img-responsive" /></a></div>
                        <div class="caption" style="min-height: 50px">
                            <h5><a style="text-decoration: none" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></h5>
                        </div>
                    </div>-->

            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 wow fadeInUpSmall">
                <a href="<?php echo $category['href']; ?>" class="img-caption">
                    <figure>
                        <img class="image img-responsive" src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>">
                        <figcaption class="caption">
                            <h3><?php echo $category['name']; ?></h3>
                        </figcaption>
                    </figure>
                </a>
            </div>

            <?php } ?>

        </div>
    </div>

</section>