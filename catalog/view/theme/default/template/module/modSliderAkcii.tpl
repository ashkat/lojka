<!--<div id="modSliderAkcii<?php echo $module; ?>" class="text-left bg-scn-stl">-->
<section id="content-block" class="text-left bg-scn-stl">
    <div class="container-fluid">
              <div class="row">

    			  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 newspay pr-0 wow fadeInLeftSmall">

                      <div id="mnCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                              <li data-target="#mnCarousel" data-slide-to="0" class="active"></li>
                              <li data-target="#mnCarousel" data-slide-to="1" class=""></li>
                              <li data-target="#mnCarousel" data-slide-to="2" class=""></li>
                              <li data-target="#mnCarousel" data-slide-to="3" class=""></li>
                              <li data-target="#mnCarousel" data-slide-to="4" class=""></li>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                              <div class="item active">
                                  <img class="one-slide" src="image/catalog/content/slmn_1.jpg" alt="Second slide">
                                  <div class="container">
                                      <div class="caption wow fadeInLeft" data-animation-effect="fadeInLeft" data-effect-delay="600">
                                          <h2>Новые поступления</h2>
                                          <p>Пирожное: Лесная ягода<br>
                                              <span>12 000</span>
                                          </p>
                                      </div>
                                  </div>
                              </div>
                              <div class="item">
                                  <img class="second-slide" src="image/catalog/content/slmn_2.jpg" alt="Second slide">
                                  <div class="container">
                                      <div class="caption wow fadeInLeft">
                                          <h2>Новые поступления</h2>
                                          <p>Штрудель: Осенний<br>
                                              <span>25 000</span>
                                          </p>
                                      </div>
                                  </div>
                              </div>
                              <div class="item">
                                <img class="third-slide" src="image/catalog/content/slmn_3.jpg" alt="Third slide">
                                <div class="container">
                                    <div class="caption wow fadeInLeft">
                                        <h2>Новые поступления</h2>
                                        <p>Рулет: Шоколадная клубника<br>
                                            <span>63 000</span>
                                        </p>
                                    </div>
                                </div>
                              </div>
                              <div class="item">
                                  <img class="four-slide" src="image/catalog/content/slmn_4.jpg" alt="Four slide">
                                  <div class="container">
                                      <div class="caption wow fadeInLeft">
                                          <h2>Новые поступления</h2>
                                          <p>Пончик: Лазурный<br>
                                              <span>7 000</span>
                                          </p>
                                      </div>
                                  </div>
                              </div>
                              <div class="item">
                                  <img class="five-slide" src="image/catalog/content/slmn_5.jpg" alt="Five slide">
                                  <div class="container">
                                      <div class="caption wow fadeInLeft">
                                          <h2>Новые поступления</h2>
                                          <p>Торт: Клубничка<br>
                                              <span>105 000</span>
                                          </p>
                                      </div>
                                  </div>
                              </div>
                            </div>
                      </div>

                  </div>

                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                      <div class="row">

                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 akcii pr-5 wow fadeInDownSmall">
                          <a href="#" data-toggle="modal" data-target="#modalProduct">
                            <img src="image/catalog/content/ptich.jpg">

                            <div class="caption wow fadeInLeftSmall">
                                <h2>Акция</h2>
                            </div>

                            <div class="captiontwo wow fadeInLeftSmall">
                                <p> Торт: Птичье молоко<br>
                                    <span>102 900</span>
                                </p>
                            </div>
                          </a>

                      </div>

                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 akcii pl-5 wow fadeInDownSmall">

                          <img src="image/catalog/content/polet.jpg">

                          <div class="caption wow fadeInLeftSmall lm10">
                              <h2>Акция</h2>
                          </div>

                          <div class="captiontwo wow fadeInLeftSmall lm10">
                              <p> Торт: Ленинградский<br>
                                  <span>81 900</span>
                              </p>
                          </div>

                      </div>
                      </div>

                  </div>

                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 lider wow fadeInUpSmall">

                      <img src="image/catalog/content/lider.jpg">

                      <div class="caption wow fadeInLeftSmall">
                          <h2>Лидер продаж</h2>
                      </div>

                      <div class="captiontwo wow fadeInLeftSmall">
                          <p> Торт: Сладкое золото<br>
                              <span class="hidden-xs">75 600</span>
                          </p>
                      </div>

                  </div>



              </div>
   </div>


</section>