<nav>
          <ul class="nav nav-pills nav-stacked">
            <?php foreach ($categories as $category) { ?>
             <?php if ($category['category_id'] == $category_id) { ?>

            <li class="active"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?><span class="badge"><?php echo $category['cnt_product']; ?></span></a></li>

            <?php if ($category['children']) { ?>
             <?php foreach ($category['children'] as $child) { ?>
             <?php if ($child['category_id'] == $child_id) { ?>

            <li class="active"><a href="<?php echo $child['href']; ?>">&nbsp;&nbsp;&nbsp;<?php echo $child['name']; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $child['href']; ?>">&nbsp;&nbsp;&nbsp;<?php echo $child['name']; ?></a></li>
            <?php } ?>
             <?php } ?>
             <?php } ?>
             <?php } else { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?><span class="badge"><?php echo $category['cnt_product']; ?></span></a></li>
              <?php } ?>
                <?php } ?>
          </ul>
</nav>